#!/bin/bash
refresh() {
    local url="$1"
    local dir="$2"

    rm -rf $dir
    git clone $url $dir
    rm -rf $dir/.git

    if [ -f "$dir/.gitignore" ]; then
        rm "$dir/.gitignore"
    fi
}

refresh https://github.com/kchmck/vim-coffee-script.git vim-coffee-script
