#!/bin/sh

SCREENS_STR=`xrandr --listactivemonitors  | cut -d' ' -f6`
SCREENS=(${SCREENS_STR[@]})

SCR1=eDP1
SCR2=DP2-1

MODE=$1

if [ $MODE = "single" ];
then
    xrandr --output $SCR1 --primary --auto --scale 1x1 --output $SCR2 --off
else
    xrandr --output $SCR1 --primary --auto --scale 0.75x0.75 --output $SCR2 --auto --$MODE $SCR1
fi


