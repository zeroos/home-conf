#!/usr/bin/env python
import dbus, sys

try:
	msg = sys.argv[1]
except IndexError:
	msg = "Zaraz wracam."


bus = dbus.SessionBus()
obj = bus.get_object("im.pidgin.purple.PurpleService", "/im/pidgin/purple/PurpleObject")
purple = dbus.Interface(obj, "im.pidgin.purple.PurpleInterface")

for conv in purple.PurpleGetIms():
    purple.PurpleConvImSend(purple.PurpleConvIm(conv), msg)
