#!/usr/bin/env python

import dbus, gtk, getopt, sys

class inputDialog(gtk.Dialog):
	def __init__(self):
		gtk.Dialog.__init__(self)
		self.set_title("Infobot")
		self.entry = gtk.Entry()
		self.vbox.pack_start(self.entry)
		self.ret = None

		self.connect("destroy", self.quit)
		self.connect("delete_event", self.quit)
		self.entry.connect("activate", self.act)

		self.entry.show()
		self.show()
		gtk.main()
	def quit(self, w=None, event=None):
		self.hide()
		self.destroy()
		gtk.main_quit()
	def act(self, w=None, event=None):
		self.ret = self.entry.get_text()
		self.quit()


win = inputDialog()
if win.ret == None or win.ret == "":
	quit()

bus = dbus.SessionBus()
obj = bus.get_object("im.pidgin.purple.PurpleService", "/im/pidgin/purple/PurpleObject")
purple = dbus.Interface(obj, "im.pidgin.purple.PurpleInterface")

purple.PurpleConvImSend(purple.PurpleConvIm(purple.PurpleConversationNew(1, purple.PurpleAccountsGetAllActive()[0], "3217426@gg.jabber.wroc.pl")), win.ret)
