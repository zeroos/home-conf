filetype off
call pathogen#helptags()
call pathogen#infect()
filetype plugin indent on

" powerline
set rtp += "~/.vim/bundle/powerline/bindings/vim"

syntax on
set cin
set mouse=a
set title
set hlsearch
set softtabstop=4
set shiftwidth=4
set tabstop=4
set expandtab
set textwidth=80
highlight Pmenu ctermfg=gray ctermbg=black


let g:templates_directory = ["~/.vim/templates"]

"" Use deoplete.
"let g:deoplete#enable_at_startup = 1


" syntastic
let g:syntastic_c_compiler = 'clang'
let g:syntastic_c_compiler_options = ' -std=c++11'
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11'

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_tex_checkers = ['chktex']

" gvim optinons
set guioptions=-m

" python-mode
let g:pymode_python = 'python3'
let g:pymode_lint_checkers = []   " ['pyflakes', 'pep8', 'pep257']

" pdflatex
let g:Tex_DefaultTargetFormat = 'pdf'
let mapleader = ','
set spelllang=en_us 
set spell

" NeoTex
let g:neotex_subfile=1

" LaTeX
function! MyFormatExpr(start, end)
    silent execute a:start.','.a:end.'s/\([^\n]\)\n\([^\n]\)/\1 \2/g|norm!``'
    silent execute a:start.','.a:end.'s/[.!?]\zs /\r/g|norm!``'
endfunction
au BufNewFile,BufReadPost *.tex setl formatexpr=MyFormatExpr(v:lnum,v:lnum+v:count-1)
au BufNewFile,BufReadPost *.tex setl fo=""
au BufNewFile,BufReadPost *.tex setl textwidth=0
au BufNewFile,BufReadPost *.html setl textwidth=200

"ipdb abbreviatoion
nnoremap <leader>p iimport ipdb; ipdb.set_trace()<esc>==


let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1 

au BufNewFile,BufReadPost *.js setl foldmethod=indent nofoldenable
au BufNewFile,BufReadPost *.js setl shiftwidth=2 softtabstop=2 tabstop=2 expandtab
au BufNewFile,BufReadPost *.ccss setl foldmethod=indent nofoldenable
au BufNewFile,BufReadPost *.scss setl shiftwidth=2 softtabstop=2 tabstop=2 expandtab
au BufNewFile,BufReadPost *.html setl textwidth=0

set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000
set undofile

set backupdir=$HOME/.vim/swp
set directory=$HOME/.vim/swp

" nice bash-like filename auto-complete
set wildmode=longest,list,full
set wildmenu

" reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

" force saving files that require root permission
cmap w!! %!sudo tee > /dev/null %

" use kj as <Esc> alternative
inoremap kj <Esc>

" Remove <F1> mapping
noremap <F1> <Esc>

" start of default statusline
set statusline=%f\ %h%w%m%r

" Syntastic statusline
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" end of default statusline (with ruler)
set statusline+=%=%(%l,%c%V\ %=\ %P%)


" neosnippet
"
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)


set termguicolors
colorscheme base16-default-dark
au BufNewFile,BufReadPost *.tex colorscheme base16-tomorrow
