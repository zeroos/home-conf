HOMEREPO=$HOME/.home-conf
HOST=$(hostname)

for dotfile in $HOMEREPO/shared/.* $HOMEREPO/host-$HOST/.* $HOMEREPO/shared/* $HOMEREPO/host-$HOST/*
do
    target=$HOME/$(basename $dotfile)
    [ -r $dotfile ] && [ ! -r $target ] && ln -s $dotfile $target
done

for dotfile in $HOMEREPO/shared/.config/.* $HOMEREPO/host-$HOST/.config/.* $HOMEREPO/shared/.config/* $HOMEREPO/host-$HOST/.config/*
do
    target=$HOME/.config/$(basename $dotfile)
    [ -r $dotfile ] && [ ! -r $target ] && ln -s $dotfile $target
done
